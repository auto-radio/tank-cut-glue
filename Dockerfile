FROM python:3.10-slim
LABEL maintainer="Loxbie <ole@freirad.at>"

ENV AURA_UID=2872
ENV AURA_GID=2872


# System Dependencies

RUN apt-get update && apt-get -y install \
      apt-utils \
      alsa-utils \
      build-essential \
      sox

RUN pip install poetry

# Setup tank-cut-glue
ENV TZ=Europe/Vienna
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /srv
RUN mkdir -p /srv/src
RUN mkdir -p /srv/config
RUN mkdir -p /srv/logs
RUN mkdir -p /var/audio/recordings/show

COPY poetry.lock /srv
COPY pyproject.toml /srv
COPY src /srv/src
COPY config/for-docker.yaml /srv/config/tank-cut-glue.yaml
COPY config/gunicorn.conf.py /srv/config/gunicorn.conf.py
COPY README.md /srv
COPY Makefile /srv

# Update Permissions
RUN groupadd --gid ${AURA_GID} aura && \
      useradd --gid ${AURA_GID} --no-user-group --uid ${AURA_UID} --home-dir /srv --no-create-home aura && \
      chown -R ${AURA_UID}:${AURA_GID} /srv /var/audio

USER aura

WORKDIR /srv
RUN poetry install --without dev --no-interaction

EXPOSE 8080

# Start tank-cut-glue
ENTRYPOINT ["make"]
CMD ["run"]