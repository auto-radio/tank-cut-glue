"""Unit tests for the ControlService using pytest."""
from datetime import datetime
from typing import List

import pytest

import tank_cut_glue.base.config as base_config

test_config = base_config.create_config("tests/config/tank-cut-glue.yaml")
base_config.instance = test_config

import tank_cut_glue.control as control  # noqa: E402
import tank_cut_glue.timeslot as ts  # noqa: E402


@pytest.fixture
def timeslots() -> List[ts.Timeslot]:
    """Fixture to return a list of timeslots without a recording."""
    id = 42
    show = 42
    schedule = 666

    memo = "This is a memo"
    repetition_of = 42
    playlist_id = 42
    note_id = 42

    timeslots = []
    for i in range(24):
        start = datetime.strptime(f"2023-01-01_{i}_00", "%Y-%m-%d_%H_%M")
        end = datetime.strptime(f"2023-01-01_{i}_59", "%Y-%m-%d_%H_%M")
        timeslot_data = {
            "id": id + i,
            "showId": show,
            "scheduleId": schedule,
            "start": start.isoformat(),
            "end": end.isoformat(),
            "memo": memo,
            "repetitionOfId": repetition_of,
            "playlistId": playlist_id,
            "noteId": note_id,
        }
        timeslots.append(ts.Timeslot(timeslot_data))
    print(timeslots)
    return timeslots


def test_check_undone_timeslots(timeslots: List[ts.Timeslot]):
    """Test the check for undone timeslots."""
    service = control.ControlService()
    start = datetime.strptime("2023-01-01_00_00", "%Y-%m-%d_%H_%M")
    end = datetime.strptime("2023-01-01_23_59", "%Y-%m-%d_%H_%M")
    monkeypatch = pytest.MonkeyPatch()
    monkeypatch.setattr(
        "tank_cut_glue.timeslot.TimeslotService.elapsed_timeslots",
        lambda start, end, test: timeslots,
    )
    undone_ts = service.get_undone_timeslots(start, end)
    assert undone_ts == timeslots
