"""Unit tests for Recording using pytest."""
from datetime import datetime

import pytest
import requests

import tank_cut_glue.base.config as base_config

test_config = base_config.create_config("tests/config/tank-cut-glue.yaml")
base_config.instance = test_config
import tank_cut_glue.timeslot as ts  # noqa: E402

AURA_CONFIG = base_config.get_config()


@pytest.fixture
def ts_data() -> dict:
    """Fixture for timeslot data."""
    id = 42
    show = 42
    schedule = 666
    start = datetime.strptime("2023-01-01_10_30", "%Y-%m-%d_%H_%M")
    end = datetime.strptime("2023-01-01_11_30", "%Y-%m-%d_%H_%M")
    memo = "This is a memo"
    repetition_of = 42
    playlist_id = 42
    note_id = 42

    timeslot_data = {
        "id": id,
        "showId": show,
        "scheduleId": schedule,
        "start": start.isoformat(),
        "end": end.isoformat(),
        "memo": memo,
        "repetitionOfId": repetition_of,
        "playlistId": playlist_id,
        "noteId": note_id,
    }
    return timeslot_data


@pytest.fixture
def timeslot(ts_data: dict) -> ts.Timeslot:
    """Fixture for creating a list of AudioBlock objects."""
    timeslot = ts.Timeslot(ts_data)
    return timeslot


def test_timeslot_formats(timeslot: ts.Timeslot):
    """Test the string method of timeslot."""
    assert timeslot.__repr__() == "Timeslot(id=42)"
    assert f"{timeslot}" == "Timeslot 42:\t2023-01-01 10:30:00 - 2023-01-01 11:30:00"


@pytest.fixture
def timeslot_service() -> ts.TimeslotService:
    """Fixture for a TimeslotService."""
    service = ts.TimeslotService()
    return service


def test_ts_steering_request(timeslot_service: ts.TimeslotService):
    """Test the steering_request method."""
    monkeypatch = pytest.MonkeyPatch()
    # create a dummy Response object
    response = requests.Response()
    response.status_code = 404
    response.url = "/this/is/a/test/url"
    response.reason = "Intended failure for testing."
    # mock requests.get()
    monkeypatch.setattr("requests.get", lambda url, params: response)
    with pytest.raises(ts.SteeringRequestFailedException):
        timeslot_service.steering_request("/test_url", None)

    # mock status code in order to succeed requests.get
    response.status_code = 200
    # mock the response in json
    monkeypatch.setattr("requests.Response.json", lambda x: "json_data")
    resp_data = timeslot_service.steering_request("/test_url")
    assert resp_data == "json_data"


def test_ts_get_timeslot(ts_data: dict, timeslot_service: ts.TimeslotService):
    """Test the get_timeslot method."""
    monkeypatch = pytest.MonkeyPatch()
    monkeypatch.setattr(
        "tank_cut_glue.timeslot.TimeslotService.steering_request",
        lambda url, params: ts_data,
    )
    returned_timeslot = timeslot_service.get_timeslot(42)
    if returned_timeslot is None:
        pytest.fail("Returned timeslot is None.")
    assert returned_timeslot.id == 42

    def monkey_raise(ex):
        raise ex

    resp = requests.Response()
    resp.status_code = 404
    resp.reason = "Intended failure for testing."
    resp.url = "/this/is/a/test/url"
    monkeypatch.setattr(
        "tank_cut_glue.timeslot.TimeslotService.steering_request",
        lambda url, params: monkey_raise(ts.SteeringRequestFailedException(resp)),
    )
    assert timeslot_service.get_timeslot(42) is None

    with pytest.raises(ts.SteeringRequestFailedException):
        resp.status_code = 500
        timeslot_service.get_timeslot(42)
