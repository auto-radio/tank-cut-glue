"""Unittests for Recording using pytest."""
from datetime import datetime
from typing import List

import pytest

import src.tank_cut_glue.base.config as base_config

test_config = base_config.create_config("tests/config/tank-cut-glue.yaml")
base_config.instance = test_config
from src.tank_cut_glue.cut_glue import AudioBlock, Recording  # noqa: E402

AURA_CONFIG = base_config.get_config()


@pytest.fixture
def audio_blocks() -> List[AudioBlock]:
    """Fixture for creating a list of AudioBlock objects."""
    start_date = datetime.strptime("2023-01-01_10_30", "%Y-%m-%d_%H_%M")
    return [AudioBlock(start_date)]


@pytest.fixture
def recording() -> Recording:
    """Fixture for creating a Recording object."""
    from_date = datetime.strptime("2023-01-01_10_40", "%Y-%m-%d_%H_%M")
    to_date = datetime.strptime("2023-01-01_10_55", "%Y-%m-%d_%H_%M")
    return Recording(from_date, to_date)


def test_recording_init(recording: Recording, audio_blocks: List[AudioBlock]):
    """Test that the Recording object is correctly initialized."""
    assert recording.audio_blocks == audio_blocks
    assert recording.from_date == datetime.strptime("2023-01-01_10_40", "%Y-%m-%d_%H_%M")
    assert recording.to_date == datetime.strptime("2023-01-01_10_55", "%Y-%m-%d_%H_%M")
    assert recording.file_location == f"{AURA_CONFIG.recording.location}/20230101_1040-1055.flac"


def test_recording_duration(recording: Recording):
    """Test that the duration property of the Recording object returns the correct value."""
    assert recording.duration == 900


def test_recording_str(recording: Recording):
    """Test that the str method of the Recording object returns the correct value."""
    assert str(recording) == "Recording 2023-01-01 10:40:00 - 2023-01-01 10:55:00, 900 seconds."


def test_recording_exists(recording: Recording):
    """Test that the exists property returns a correct value."""
    monkeypatch = pytest.MonkeyPatch()
    monkeypatch.setattr("os.path.exists", lambda x: True)
    assert recording.exists
    monkeypatch.setattr("os.path.exists", lambda x: False)
    assert not recording.exists


def test_recording_status(recording: Recording):
    """Test that the status property returns the expected value."""
    monkeypatch = pytest.MonkeyPatch()
    monkeypatch.setattr("os.path.exists", lambda x: True)
    assert (
        recording.status[1]
        == f"Recording is stored in {AURA_CONFIG.recording.location}/20230101_1040-1055.flac"
    )
    assert recording.status[0] == 200

    monkeypatch.setattr("os.path.exists", lambda x: False)
    assert recording.status[0] == 102
    assert recording.status[1] == (
        f"Blocks [{AURA_CONFIG.audio_block.store_dir}/"
        "2023/01/01/2023-01-01_10:30:00_1672565400.flac] are not ready."
    )

    monkeypatch.setattr("src.tank_cut_glue.cut_glue.AudioBlock.exists", lambda x: True)
    assert recording.status[0] == 202
    assert recording.status[1] == "Recording not yet ready."


def test_recording_offset(recording: Recording):
    """Test that the recording calculates the offset correctly."""
    assert recording.offset == 600
