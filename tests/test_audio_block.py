"""Unit tests for AudioBlock using pytest."""
from datetime import datetime, timezone

import pytest
from pytest_mock import MockerFixture

import tank_cut_glue.base.config as base_config

test_config = base_config.create_config("tests/config/tank-cut-glue.yaml")
base_config.instance = test_config
from tank_cut_glue.cut_glue import AudioBlock  # noqa: E402

AURA_CONFIG = base_config.get_config()


@pytest.fixture
def audio_block():
    """Fixture for creating an AudioBlock instance."""
    start_date = datetime(2022, 1, 1, 0, 0, 0, 0, timezone.utc)
    return AudioBlock(start_date)


def test_init(audio_block: AudioBlock):
    """Tests that the AudioBlock instance is initialized correctly."""
    assert audio_block.start_date == datetime(2022, 1, 1, 0, 0, 0, 0, timezone.utc)

    # Check that the s_time attribute is set correctly
    assert audio_block.s_time == "1640991600"

    print(audio_block.start_date.astimezone(timezone.utc))

    # Check that the path attribute is set correctly
    assert (
        audio_block.path
        == f"{AURA_CONFIG.audio_block.store_dir}/2022/01/01/2022-01-01_00:00:00_1640991600.flac"
    )
    assert (
        str(audio_block)
        == f"{AURA_CONFIG.audio_block.store_dir}/2022/01/01/2022-01-01_00:00:00_1640991600.flac"
    )


def test_exists(audio_block: AudioBlock, mocker: MockerFixture):
    """Tests that the exists property of the AudioBlock instance returns the correct value."""
    # Set up a mock os.path.exists function that always returns False
    mocker.patch("os.path.exists", return_value=False)

    # Check that the exists property returns False
    assert not audio_block.exists
    assert not audio_block
