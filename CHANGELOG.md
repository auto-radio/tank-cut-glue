# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [1.0.0-alpha2] - 2023-06-21

Initial release. This starts at 1.0.0-alpha2 to be in sync with the other components. There is a exception when exiting tank-cut-glue when using gunicorn with gthread. Sometime you have to press `Ctrl+C` twice to exit #27.