# tank-cut-glue

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/tank-cut-glue/-/badges/release.svg)](https://gitlab.servus.at/aura/tank-cut-glue/-/releases) [![pipeline status](https://gitlab.servus.at/aura/tank-cut-glue/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/tank-cut-glue/-/commits/main) [![coverage report](https://gitlab.servus.at/aura/tank-cut-glue/badges/main/coverage.svg)](https://gitlab.servus.at/aura/tank-cut-glue/-/commits/main) 

`tank-cut-glue` is a tool that allows you to cut the recordings from the [engine-recorder](https://gitlab.servus.at/aura/engine-recorder) into individual episodes. This means that `tank-cut-glue` is able to glue audio blocks together and cut an audio file from this using a start and end date. It is meant to run together with a `engine-recorder` on one machine but I is also possible to run separate given it has access to the audio blocks via a network filesystem. Optionally `tank-cut-glue` can check the [steering timeslot api](https://api.aura.radio/steering/#/timeslots) for past timeslots and create episodes from the recordings provided by `engine-recorder`. 

It uses [sox](https://linux.die.net/man/1/sox) in the background for the hard lifting and a flask gunicorn server for the REST API.

`tank-cut-glue` provides two endpoints, reachable over http:

- `/recording?`
- `/recording/{timeslot_id}/`

To learn more about the API Specs of this project have a look at the [Swagger Specification](https://api.aura.radio/).

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed the latest version of: `Docker`.
* You have a `Linux` machine. `tank-cut-glue` has been tested on `Debian 11`.
* You have access to the audio blocks from `engine-recorder` or any other form of continuous audio recording over the filesystem.

## Installing tank-cut-glue using Docker

For production deployments follow the Docker Compose installation instruction for AURA at [docs.aura.radio](https://docs.aura.radio).


## Installing Engine-Recorder for development.

The following instructions are meant for development.

### Build with Docker

Build your own, local Docker image

```bash
make docker.build
```

### Configuration

Check out the sample file in `config/for-docker.yaml`. Copy this to `config/tank-cut-glue.yaml`, change this file to your needs. Also check out the gunicorn settings in `/config/gunicorn.conf.py` if you want to use gunicorn.

### Run with Docker

Run the locally build image with gunicorn

```bash
make docker.run
```

If you want a quick dev server you can use the werkzeug server as well

```bash
make docker.run.dev
```

### More Docker Options

Check for more Docker things with

```bash
make help
```

## Installing tank-cut-glue bare metal

To install tank-cut-glue bare metal, follow these steps:

### Requirements

- `git`, `make`
- [Python 3.10+](https://www.python.org/downloads/release/python-3100/)
- [Poetry](https://python-poetry.org/)

After that install all dependencies:

```
make init.app
```

For development install with:

```
make init.dev
```

### Configuration

Check out the sample configuration file in `config/sample-tank-cut-glue.yaml`. Copy this to `config/tank-cut-glue.yaml` and change it to your needs. Also check out the gunicorn settings in `/config/gunicorn.conf.py` if you want to use gunicorn.

## Using tank-cut-glue

To use tank-cut-glue, follow this simple step:

```
make run
```

This starts the tank-cut.glue and after a short startup time it endpoints will be reachable. On a local (docker) dev build you can check out the swagger ui at [localhost:8080/api/v1/ui](http://localhost:8080/api/v1/ui/).

## Read more

- [Developer Guide](docs/developer-guide.md)
- [docs.aura.radio](https://docs.aura.radio)
- [aura.radio](https://aura.radio)