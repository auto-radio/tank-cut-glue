"""Tank Cut and Glue App."""

import importlib.metadata
import signal
import sys

import connexion

import tank_cut_glue.base.config as base_config
from tank_cut_glue.api import encoder
from tank_cut_glue.base.logger import logger
from tank_cut_glue.control import ControlService
from tank_cut_glue.cut_glue import CutGlueService

AURA_CONFIG = base_config.get_config()

control_job = None


def shutdown(signal_number, frame):
    """Called when the application shuts down."""
    logger.debug(f"Signal {signal_number} intercepted! Trying to exit now...")
    if control_job:
        logger.debug("Shutdown Control Service.")
        control_job.exit()
    sys.exit(0)


# atexit.register(shutdown)

# catch Ctrl+C and call shutdown
signal.signal(signal.SIGINT, shutdown)

app = connexion.App(__name__, specification_dir="api/openapi")
app.app.json_encoder = encoder.JSONEncoder  # type: ignore
app.app.config["SERVICE"] = CutGlueService()  # type: ignore
app.add_api("openapi.yaml", arguments={"title": "AURA Tank-Cut-Glue API"}, pythonic_params=True)
port = AURA_CONFIG.apis.port
# app.run(port=port)


def main():
    """Main function to start the app."""
    port = AURA_CONFIG.apis.port
    app.run(host="0.0.0.0", port=port)


with app.app.app_context():  # type: ignore
    version = importlib.metadata.version("tank-cut-glue")
    logger.info(f"tank-cut-glue {version} started.")

    if AURA_CONFIG.control.enabled:
        control_job = ControlService()
        control_job.start()


if __name__ == "__main__":
    main()
