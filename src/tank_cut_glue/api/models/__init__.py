# coding: utf-8

# flake8: noqa
from __future__ import absolute_import

# import models into model package
from tank_cut_glue.api.models.audio_file import AudioFile
