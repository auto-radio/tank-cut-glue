"""Encoder."""
import six
from connexion.apps.flask_app import FlaskJSONEncoder

from tank_cut_glue.api.models.base_model_ import Model


class JSONEncoder(FlaskJSONEncoder):
    """JSON Encoder."""

    include_nulls = False

    def default(self, o):
        """Default encoding."""
        if isinstance(o, Model):
            dikt = {}
            for attr, _ in six.iteritems(o.openapi_types):
                value = getattr(o, attr)
                if value is None and not self.include_nulls:
                    continue
                attr = o.attribute_map[attr]
                dikt[attr] = value
            return dikt
        return FlaskJSONEncoder.default(self, o)
