"""Controller to handle internal requests."""

from flask import current_app

import tank_cut_glue.cut_glue as cg
from tank_cut_glue.api import util
from tank_cut_glue.api.models.audio_file import AudioFile  # noqa: E501


def cut_glue_endpoint(from_date, to_date):  # noqa: E501
    """Return file location of the finished recording.

    Returns the location of a audio file which is cut and glued out of the recording blocks for a
     given time span.  # noqa: E501

    :param fromDate: Start point for the audio file
    :type fromDate: str
    :param toDate: Endpoint for the audio file
    :type toDate: str

    :rtype: List[AudioFile]
    """
    fromDate = util.deserialize_datetime(from_date)
    toDate = util.deserialize_datetime(to_date)
    cg_service = current_app.config["SERVICE"]  # type: cg.CutGlueService
    recording = cg_service.cut_glue(fromDate, toDate)
    if recording is None:
        return AudioFile(False)

    audio_file = AudioFile(
        True,
        recording.file_location,
        recording.from_date.isoformat(),
        recording.to_date.isoformat(),
    )
    return audio_file


def cut_glue_timeslot_id_endpoint(timeslot_id):  # noqa: E501
    """Audio file for given timeslot.

    Returns the audio file location for a file matching the duration of the timeslot  # noqa: E501

    :param timeslot_id: ID of the timeslot
    :type timeslot_id: int

    :rtype: List[AudioFile]
    """
    cg_service = current_app.config["SERVICE"]  # type: cg.CutGlueService
    recording = cg_service.cut_glue_timeslot_id(int(timeslot_id))
    if recording is None:
        return AudioFile(False)
    audio_file = AudioFile(
        True,
        recording.file_location,
        recording.from_date.isoformat(),
        recording.to_date.isoformat(),
    )
    return audio_file
