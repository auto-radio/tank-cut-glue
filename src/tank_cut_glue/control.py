"""Module to control the periodical process of timeslots."""
import threading
from datetime import datetime, timedelta
from typing import List

import tank_cut_glue.base.config as base_config
from tank_cut_glue.base.logger import logger
from tank_cut_glue.cut_glue import CutGlueService, Recording
from tank_cut_glue.timeslot import Timeslot, TimeslotService

AURA_CONFIG = base_config.get_config()


class ControlService(threading.Thread):
    """Control instance to process timeslots."""

    def __init__(self):
        """Init the control service."""
        threading.Thread.__init__(self)
        self.exit_event = threading.Event()
        self.ts = TimeslotService()
        self.cgs = CutGlueService()
        self.interval = AURA_CONFIG.control.interval

    def get_undone_timeslots(self, start: datetime, end: datetime) -> List[Timeslot]:
        """Returns timeslots which are not processed yet.

        Meaning they do not have a saved recording file.
        """
        undone_timeslots = []
        timeslots = self.ts.elapsed_timeslots(start, end)
        for timeslot in timeslots:
            if not self._timeslot_has_recording(timeslot):
                undone_timeslots.append(timeslot)
        return undone_timeslots

    def _timeslot_has_recording(self, timeslot: Timeslot) -> bool:
        recording = Recording(timeslot.start, timeslot.end)
        return recording.exists

    def process_timeslots(self):
        """Starts a cut and glue job for undone timeslots."""
        start = datetime.now() - timedelta(hours=72)
        end = datetime.now()
        logger.info(
            f"Checking timeslots from {start.strftime('%Y-%m-%d %H:%M')}"
            f" to {end.strftime('%Y-%m-%d %H:%M')}"
        )
        undone_timeslots = self.get_undone_timeslots(start, end)
        for timeslot in undone_timeslots:
            logger.info(f"Start process for timeslot {timeslot.id}")
            rec = self.cgs.cut_glue(timeslot.start, timeslot.end)
            if rec is None:
                logger.warning(f"Timeslot {timeslot.id} could not be processed.")
            else:
                logger.info(
                    f"Recording for timeslot {timeslot.id} is saved at '{rec.file_location}'."
                )

    def run(self):
        """Start the Control Service."""
        while not self.exit_event.is_set():
            logger.debug("Run pending Control Jobs.")
            self.process_timeslots()
            self.exit_event.wait(AURA_CONFIG.control.interval)

    def exit(self):
        """Shutdown the control thread."""
        self.exit_event.set()


def main():
    """Run the control service.

    This can be done without the REST API.
    """
    cs = ControlService()
    cs.run()
