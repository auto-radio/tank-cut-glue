"""MetadataService to retrieve information from engine-api."""

from datetime import datetime
from typing import List

import requests


class RequestFailedException(Exception):
    """An exception raised when a request to the aura endpoint fails.

    Attributes:
        response: The failed HTTP response.
    """

    def __init__(self, response: requests.models.Response):
        """Create a new RequestFailedException instance.

        Args:
            response: The failed HTTP response.
        """
        self.response = response
        exception_msg = (
            f"Error: Request to {self.response.url}"
            f"failed with {self.response.status_code}: {self.response.reason}"
        )
        super().__init__(exception_msg)


class Metadata:
    """Specific Information about a episode/recording."""

    def __init__(self, raw: dict):
        """Create a new Metadata instance from a dictionary.

        Args:
            raw: A dictionary with keys
                "track_album", "show_id", "playlist_id", "track_start", "track_title", "track_num",
                "show_name", "track_artist", "timeslot_id", "track_duration" and "track_type".
        """
        self.playlist_id = int(raw["playlist_id"])
        self.timeslot_id = int(raw["timeslot_id"])
        self.show_id = int(raw["show_id"])
        self.show_name = raw["show_name"]
        self.track_album = raw["track_album"]
        self.track_artist = raw["track_artist"]
        self.track_duration = int(raw["track_duration"])
        self.track_num = int(raw["track_num"])
        self.track_title = raw["track_title"]
        self.track_type = raw["track_type"]
        self.track_start = raw["track_start"]


class MetadataService:
    """Service to retrieve Metadata information from the Engine API."""

    def __init__(self, engine_api_url: str) -> None:
        """Initialize the MetadataService."""
        self.engine_api_url = engine_api_url

    def engine_request(self, params=None):
        """Send a GET request to Engine-API with optional query parameters.

        Args:
            params: optional parameters in form of a dict for the request.

        Returns:
            A list containing dicts representing json objects.

        Raises:
            RequestFailedException: When a request returns with a status code other then 200.
        """
        response = requests.get(self.engine_api_url, params=params)

        if response.status_code != 200:
            raise RequestFailedException(response)
        else:
            data = response.json()
            return data

    def metadata(self, from_date: datetime, to_date: datetime) -> List[Metadata] | None:
        """Get the metadata for a given time span from the ENGINE API.

        Args:
            from_date: The date to start from.
            to_date: The date where to stop.

        Returns:
            A list containing Metadata objects.
        """
        params = {
            "from_date": from_date.strftime("%Y-%m-%d"),
            "to_date": to_date.strftime("%Y-%m-%d"),
        }
        try:
            metadata_data = self.engine_request(params=params)
        except RequestFailedException as rfe:
            print(f"Request failed. {rfe}")
            return None

        metadata_list = []
        for raw_metadata in metadata_data:
            metadata_list.append(Metadata(raw_metadata))
        return metadata_list
