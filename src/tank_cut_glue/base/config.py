"""Config parser for tank-cut-glue."""

import os
import re
from dataclasses import dataclass

import yaml

DEFAULT_CONFIG_PATH: str = "config/tank-cut-glue.yaml"
instance = None


@dataclass
class AudioBlock:
    """Config class representing an audio block.

    Attributes:
        path_str (str): The path to the audio block.
        file_str (str): The file name of the audio block.
        file_type (str): The file type of the audio block.
        store_dir (str): The directory where the audio block is stored.
    """

    path_str: str
    file_str: str
    file_type: str
    store_dir: str


@dataclass
class APIs:
    """Config class representing APIs.

    Attributes:
        port (int): The port on which the server will listen.
        steering_url (str): The URL for the steering API.
        engine_api_url (str): The URL for the engine API.
        tank_url (str): The URL for the tank API.
    """

    port: int
    steering_url: str
    engine_api_url: str
    tank_url: str


@dataclass
class Control:
    """Config class representing control settings.

    Attributes:
        enabled (bool): A flag indicating whether control is enabled.
        interval (int): The interval for control.
    """

    enabled: bool
    interval: int


@dataclass
class Recording:
    """Config class representing recording settings.

    Attributes:
        location (str): The location for recording.
        enable_show_name (bool): A flag indicating whether to enable show name.
        file_name (str): The name of the file for recording.
        file_type (str): The file type for recording.
        audio_block_interval (int): The interval for audio block.
    """

    location: str
    enable_show_name: bool
    file_name: str
    file_type: str
    audio_block_interval: int


@dataclass
class Logger:
    """Config class representing a logger.

    Attributes:
        log_dir (str): The directory where logs are stored.
        log_level (str): The logging level.
        name (str): The name of the logger.
        rotating_size (int): The size of the log file before it rotates.
            Default is 500000, eg 5MB.
        backup_count (int): The number of backups (rotations) to keep.
    """

    log_dir: str
    log_level: str = "INFO"
    name: str = "tank-cut-glue"
    rotating_size: int = 500000
    backup_count: int = 7


@dataclass
class Config:
    """A data class representing the configuration.

    Attributes:
        audio_block (AudioBlock): The settings for audio blocks.
        apis (APIs): The APIs settings.
        control (Control): The control settings.
        recording (Recording): The recording settings.
        logger (Logger): The logger settings.
    """

    audio_block: AudioBlock
    apis: APIs
    control: Control
    recording: Recording
    logger: Logger


def create_config(config_path=None) -> Config:
    """Initializes the Config, default is `config/tank-cut-glue.yaml`.

    This reads a yaml configuration from the `config_path` and loads this dict into
    a dataclass `Config`. The dataclass itself is build from multiple dataclasses,
    all of these take either one or multiple dataclasses, or  a dict to initialise.


    Args:
        config_path (str, optional): The path to the configuration file.

    Returns:
        Config: The created configuration.

    Raises:
        FileNotFoundError: If the specified `config_path` does not exist.
        TypeError: If the configuration could not be loaded.

    """
    path = config_path or DEFAULT_CONFIG_PATH

    if not path[0] == "/":
        base_path = os.path.dirname(
            os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        )
        config_path = os.path.join(base_path, path)
    else:
        config_path = path
    if not os.path.exists(config_path):
        print(f"Error: The config file {config_path} does not exist.")
        raise FileNotFoundError

    config_dict = _load_config(config_path)
    if config_dict is None:
        print("Error: Could not set configuration.")
        raise TypeError
    else:
        print(f"Configuration file '{config_path}' has been loaded.")
        audio_block = AudioBlock(**config_dict["AudioBlock"])
        apis = APIs(**config_dict["APIs"])
        control = Control(**config_dict["Control"])
        recording = Recording(**config_dict["Recording"])
        logger = Logger(**config_dict["Logger"])

        return Config(audio_block, apis, control, recording, logger)


def _load_config(config_path) -> dict | None:
    """Load settings from yaml file and sets default values."""
    path_matcher = re.compile(r".*\$\{([^}^{]+)\}.*")

    # expand the path with environment variables
    def path_constructor(loader, node):
        return os.path.expandvars(node.value)

    EnvVarLoader.add_implicit_resolver("!path", path_matcher, None)
    EnvVarLoader.add_constructor("!path", path_constructor)

    try:
        with open(config_path, "r") as stream:
            # use the custom yaml loader with env variables
            return yaml.load(stream, Loader=EnvVarLoader)
    except yaml.YAMLError:
        print(f"Error: A problem occurred reading the YAML config file '{config_path}'.")
    except FileNotFoundError:
        print(f"Error: Could not find '{config_path}'.")
    except PermissionError:
        print(f"Error: Permission denied while trying to access '{config_path}'.")


class EnvVarLoader(yaml.SafeLoader):
    """Wrapper class for yaml.SafeLoader."""

    pass


def get_config() -> Config:
    """Returns the configuration instance.

    Returns:
        Config: The configuration instance.
    """
    global instance
    if not instance:
        instance = create_config()
    return instance
