"""Logger for tank-cut-glue."""

import logging
import logging.handlers as handlers
import os.path
from enum import Enum

import tank_cut_glue.base.config as base_config

AURA_CONFIG = base_config.get_config()


def bold(text: str) -> str:
    """Create a bold version of the given text."""
    return f"{TerminalColors.BOLD.value}{text}{TerminalColors.ENDC.value}"


def underline(text: str) -> str:
    """Create a underlined version of the given text."""
    return f"{TerminalColors.UNDERLINE.value}{text}{TerminalColors.ENDC.value}"


def blue(text: str) -> str:
    """Create a blue version of the given text."""
    return f"{TerminalColors.BLUE.value}{text}{TerminalColors.ENDC.value}"


def red(text: str) -> str:
    """Create a red version of the given text."""
    return f"{TerminalColors.RED.value}{text}{TerminalColors.ENDC.value}"


def pink(text: str) -> str:
    """Create a red version of the given text."""
    return f"{TerminalColors.PINK.value}{text}{TerminalColors.ENDC.value}"


def yellow(text: str) -> str:
    """Create a yellow version of the given text."""
    return f"{TerminalColors.YELLOW.value}{text}{TerminalColors.ENDC.value}"


def green(text: str) -> str:
    """Create a red version of the given text."""
    return f"{TerminalColors.GREEN.value}{text}{TerminalColors.ENDC.value}"


def cyan(text: str) -> str:
    """Create a cyan version of the given text."""
    return f"{TerminalColors.CYAN.value}{text}{TerminalColors.ENDC.value}"


class TerminalColors(Enum):
    """Colors for formatting terminal output."""

    HEADER = "\033[95m"

    RED = "\033[31m"
    GREEN = "\033[32m"
    YELLOW = "\033[33m"
    BLUE = "\033[34m"
    PINK = "\033[35m"
    CYAN = "\033[36m"

    WARNING = "\033[31m"
    FAIL = "\033[41m"

    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    STRIKE = "\u0336"

    ENDC = "\033[0m"


class ColoredFormatter(logging.Formatter):
    """Logging Formatter to apply colors to log levels."""

    # set format of log
    datepart = "%(asctime)s:%(name)s:%(levelname)s"
    message = " - %(message)s"
    format_str = f"{datepart}\t{message}"

    FORMATS = {
        logging.DEBUG: blue(format_str + " - [%(filename)s:%(lineno)s-%(funcName)s()]"),
        logging.INFO: format_str,
        logging.WARNING: yellow(format_str),
        logging.ERROR: red(format_str),
        logging.CRITICAL: bold(red(format_str)),
    }

    def format(self, record):
        """Format the record with colored levels."""
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


mapping = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL,
}

log_level = mapping.get(AURA_CONFIG.logger.log_level, logging.DEBUG)


logger = logging.getLogger(AURA_CONFIG.logger.name)
logger.setLevel(log_level)

if not logger.hasHandlers():
    print(
        (
            f"Created {green(AURA_CONFIG.logger.name)} Logger, "
            f"log level: {log_level} ({AURA_CONFIG.logger.log_level})"
        )
    )
    # create file handler for logger
    path = os.path.join(AURA_CONFIG.logger.log_dir, f"{AURA_CONFIG.logger.name}.log")

    file_handler = handlers.RotatingFileHandler(
        path,
        maxBytes=AURA_CONFIG.logger.rotating_size,
        backupCount=AURA_CONFIG.logger.backup_count,
    )
    file_handler.setLevel(log_level)

    # create stream handler for logger
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(log_level)

    # File Formatter
    datepart = "%(asctime)s:%(name)s:%(levelname)s"
    message = " - %(message)s"
    format_str = f"{datepart}{message}"
    file_formatter = logging.Formatter(format_str)

    # set log of handlers
    file_handler.setFormatter(file_formatter)
    stream_handler.setFormatter(ColoredFormatter())

    # add handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    logger.debug("ADDED HANDLERS")
else:
    logger.debug("REUSED LOGGER")
