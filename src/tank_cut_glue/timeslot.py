"""Module to connect to the Steering API."""

from datetime import datetime, timedelta
from typing import List, Tuple

import requests

import tank_cut_glue.base.config as base_config
from tank_cut_glue.base.logger import logger

AURA_CONFIG = base_config.get_config()


class SteeringRequestFailedException(Exception):
    """An exception raised when a request to the steering api fails.

    Attributes:
        response: The failed HTTP response.
    """

    def __init__(self, response: requests.models.Response):
        """Create a new SteeringRequestFailedException instance.

        Args:
            response: The failed HTTP response.
        """
        self.response = response
        exception_msg = (
            f"Error: Request to {self.response.url}"
            f"failed with {self.response.status_code}: {self.response.reason}"
        )
        super().__init__(exception_msg)


class Timeslot:
    """Class to represent a timeslot."""

    def __init__(self, raw: dict):
        """Create a new Timeslot instance from a dictionary.

        Args:
            raw: A dictionary with keys "id", "showId", "scheduleId", "start", "end", "memo",
                "repetitionOfId", "playlistId", and "noteId".

        """
        self.id = int(raw["id"])
        self.show = int(raw["showId"])
        self.schedule = int(raw["scheduleId"])
        self.start = datetime.fromisoformat(raw["start"])
        # FIXME: we remove the tzinfo here, maybe there is a better way
        self.start = self.start.replace(tzinfo=None)
        self.end = datetime.fromisoformat(raw["end"])
        self.end = self.end.replace(tzinfo=None)
        self.memo = raw["memo"]
        self.repetition_of = raw["repetitionOfId"]
        self.playlist_id = raw["playlistId"]
        self.note_id = raw.get("noteId")

    def __str__(self):
        """Return a string representation of the Timeslot instance.

        Returns:
            A string in the format "Timeslot {id}: {start} - {end}".
        """
        return f"Timeslot {self.id}:\t{self.start} - {self.end}"

    def __repr__(self) -> str:
        """Return a string representation of the Timeslot instance.

        Returns:
            A string representation of the Timeslot instance.
        """
        return f"Timeslot(id={self.id})"


class TimeslotService:
    """Class to retrieve information about a timeslot."""

    def __init__(self):
        """Create a new TimeslotService instance.

        Args:
            url: The URL of the Steering API.
        """
        self.config = AURA_CONFIG.apis
        self.steering_url = self.config.steering_url
        self.timeslot_url = f"{self.steering_url}/api/v1/timeslots"
        logger.info("TimeslotService created.")

    def steering_request(self, url, params=None):
        """Send a GET request to a URL with optional query parameters.

        Args:
            url: The URL to send the request to.
            params: A dictionary of query parameters to include in the request.

        Returns:
            The JSON data contained in the response, if the request was successful.

        Raises:
            SteeringRequestFailedException: If the request failed with a status code other than
             200.
        """
        try:
            response = requests.get(url, params=params)
            if response.status_code != 200:
                logger.warning(
                    f"Received response code {response.status_code} on request {response.url}"
                )
                raise SteeringRequestFailedException(response)
            else:
                data = response.json()
                return data
        except ConnectionError as conn_error:
            logger.warning(f"Steering {self.steering_url} is not reachable.")
            logger.debug(f"Connection Error: {conn_error}")
            return None

    def get_timeslot(self, timeslot_id: int) -> Timeslot | None:
        """Retrieve a timeslot by its ID from the steering API.

        Args:
            timeslot_id: The ID of the timeslot.

        Returns:
            A Timeslot instance or None.
        """
        timeslot_id_url = f"{self.timeslot_url}/{timeslot_id}"
        logger.info(f"Requesting timeslot from '{timeslot_id_url}'.")
        try:
            data = self.steering_request(timeslot_id_url)
        except SteeringRequestFailedException as steering_exception:
            if steering_exception.response.status_code == 404:
                logger.info(f"Timeslot '{timeslot_id}' does not exist in steering.")
                return None
            else:
                raise steering_exception
        return Timeslot(data) if data else None

    def get_start_end_from_timeslot(self, timeslot_id: int) -> Tuple[datetime, datetime] | None:
        """Retrieve the start and end times of a timeslot by its ID.

        Args:
            timeslot_id: The ID of the timeslot.

        Returns:
            A tuple containing the start and end times of the timeslot as datetime objects.
        """
        timeslot = self.get_timeslot(timeslot_id)
        if timeslot is None:
            return None
        return (timeslot.start, timeslot.end)

    def elapsed_timeslots(
        self,
        start_date: datetime | None = None,
        end_date: datetime | None = None,
    ) -> List[Timeslot]:
        """Returns a list of elapsed timeslots.

        The timeslots are returned in order of end date, with the most recently ended timeslot
         first.

        Returns:
            A list of Timeslot objects.
        """
        if start_date is None:
            start_date = datetime.now() - timedelta(hours=24)
        if end_date is None:
            end_date = datetime.now()
        params = {
            "end": end_date.strftime("%Y-%m-%d"),
            "start": start_date.strftime("%Y-%m-%d"),
            "order": "end",
        }
        logger.info(
            f"Requesting timeslots from "
            f"'{self.timeslot_url}?start={start_date.strftime('%Y-%m-%d')}"
            f"&end={end_date.strftime('%Y-%m-%d')}"
            f"&order=end'"
        )
        data = self.steering_request(self.timeslot_url, params)
        # if we don't get any data from steering we return a empty list
        if data is None:
            logger.info("Steering did not return any data.")
            return []
        timeslots = []
        for timeslot_data in data:
            timeslots.append(Timeslot(timeslot_data))
        return timeslots
