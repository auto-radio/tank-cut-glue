"""Tank-Cut-Glue."""

import os
import subprocess
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import List, Tuple

import tank_cut_glue.base.config as base_config
from tank_cut_glue.base.logger import logger
from tank_cut_glue.timeslot import TimeslotService

AURA_CONFIG = base_config.get_config()


class AudioBlock:
    """An AudioBlock represents a contiguous audio recording.

    Attributes:
        start_date (datetime): The start date and time of the recording.
        s_time (str): The start time of the recording in seconds.
        path (str): The file path of the audio recording.
    """

    def __init__(self, start_date: datetime):
        """Initializes the AudioBlock.

        Args:
            start_date (datetime): The start date and time of the recording.
        """
        self.config = AURA_CONFIG.audio_block
        self.start_date = start_date
        self.s_time = start_date.strftime("%s")
        store_dir = self.config.store_dir

        file_str = self.start_date.strftime(self.config.file_str)
        file_name = f"{file_str}{self.config.file_type}"
        path_str = self.start_date.strftime(self.config.path_str)

        self.path = os.path.join(store_dir, path_str, file_name)

    @property
    def exists(self):
        """Returns True if the audio file exists, False otherwise."""
        return os.path.exists(self.path)

    def __str__(self):
        """Returns the string representation of the AudioBlock."""
        return self.__repr__()

    def __repr__(self) -> str:
        """Returns the string representation of the AudioBlock."""
        return self.path

    def __bool__(self) -> bool:
        """Returns True if the audio file exists, False otherwise."""
        return self.exists

    def __eq__(self, other):
        """Audio blocks are equal if they have the same path."""
        if isinstance(other, AudioBlock):
            return self.path == other.path


class Recording:
    """A Recording.

    Attributes:
        audio_blocks (List[AudioBlock]): A list of AudioBlock objects representing the audio
             recording.
        from_date (datetime): The start date and time of the recording.
        to_date (datetime): The end date and time of the recording.
        file_location (str): The file path of the finished audio recording.
    """

    config = AURA_CONFIG.recording

    def __init__(
        self,
        from_date: datetime,
        to_date: datetime,
    ) -> None:
        """Initializes the Recording.

        Args:
            from_date (datetime): The start date and time of the recording.
            to_date (datetime): The end date and time of the recording.
        """
        self.audio_blocks = self.build_audio_blocks(
            from_date, to_date, self.config.audio_block_interval
        )
        file_name = (
            f"{from_date.strftime(self.config.file_name)}"
            f"-{to_date.strftime('%H%M')}{self.config.file_type}"
        )
        if self.config.enable_show_name:
            # TODO: look for show name in Metadata
            # or move this when we look for Metadata anyway?
            pass

        self.file_location = os.path.join(self.config.location, file_name)
        self.from_date = from_date
        self.to_date = to_date

    def __str__(self):
        """Returns the string representation of the Recording."""
        return f"Recording {self.from_date} - {self.to_date}, {self.duration} seconds."

    @property
    def duration(self) -> int:
        """Returns the duration of the recording in seconds.

        Returns:
            int: The duration of the recording in seconds.
        """
        duration = self.to_date - self.from_date
        return int(duration.total_seconds())

    @property
    def exists(self):
        """Returns true if the finished file exists."""
        return os.path.exists(self.file_location)

    @property
    def status(self) -> Tuple[int, str]:
        """Returns a tuple containing an HTTP status code and a message.

        Returns:
            A tuple with the HTTP status code and a message. The HTTP status codes are:
                - 200: recording is ready and stored in the file location specified
                - 102: blocks are not yet ready, at least one block is missing
                - 202: recording is not yet ready, but being processed
        """
        logger.debug(f"Checking status of Recording {self.file_location}")
        if self.exists:
            return 200, f"Recording is stored in {self.file_location}"

        missing_blocks = []
        for block in self.audio_blocks:
            if not block.exists:
                missing_blocks.append(block)

        if missing_blocks != []:
            logger.debug(f"Blocks missing {missing_blocks}")
            return 102, f"Blocks {missing_blocks} are not ready."

        if not self.exists:
            return 202, "Recording not yet ready."

        return 404, "Cut and Glue Error."

    @property
    def offset(self) -> int:
        """Returns the time in seconds from the first audio block to the start time."""
        first_block = self.audio_blocks[0].start_date
        offset = self.from_date - first_block
        return int(offset.total_seconds())

    def build_audio_blocks(
        self, from_date: datetime, to_date: datetime, interval: int
    ) -> List[AudioBlock]:
        """Build the audio blocks with the interval in seconds."""
        start_block = from_date - (from_date - datetime.min) % timedelta(seconds=interval)

        audio_blocks = []
        while start_block <= to_date:
            audio_blocks.append(AudioBlock(start_block))
            start_block = start_block + timedelta(seconds=interval)

        return audio_blocks


class CutGlueService:
    """Cut-Glue Service."""

    def _build_sox_cmd(self, recording: Recording) -> List[str]:
        """Builds a sox command from a Recording.

        Args:
            recording (Recording): The recording to build the sox command for.

        Returns:
            List[str]: A list of strings representing the sox command.
        """
        sox = ["/usr/bin/sox", "-D"]
        trim = ["trim", f"{recording.offset}", f"{recording.duration}"]
        sox_cmd = [
            *sox,
            *[block.path for block in recording.audio_blocks],
            recording.file_location,
            *trim,
        ]
        return sox_cmd

    def run_sox(self, sox_cmd: List[str]) -> bool:
        """Runs a sox command to merge and cut the audio blocks together.

        Args:
            sox_cmd (List[str]): A list of strings representing the sox command.

        Returns:
            bool: True if the command ran successfully, False otherwise.
        """
        cmd_string = '" "'.join(sox_cmd[1:])
        logger.debug(f'Glue and Cut with sox: {sox_cmd[0]} "{cmd_string}"')
        try:
            subprocess.run(sox_cmd, check=True)
        except subprocess.CalledProcessError as cpe:
            logger.error(f"Executing sox failed: {cpe.returncode}")
            logger.error(cpe.stderr)
            return False
        return True

    def cut_glue(self, from_date: datetime, to_date: datetime) -> Recording | None:
        """Cuts and glues audio blocks into one single episode."""
        recording = Recording(from_date, to_date)

        logger.debug(f"Recording status: {recording.status}")
        if recording.status[0] == 102:
            logger.warning(recording.status[1])
            return None
        if recording.status[0] == 200:
            logger.info(f"Recording already exists: {recording.file_location}")
            return recording
        cmd = self._build_sox_cmd(recording)
        if self.run_sox(cmd):
            logger.debug(f"Recording is done: {recording.status}")
            if recording.status[0] == 200:
                logger.info(f"{recording.file_location} is ready.")
                return recording
        else:
            logger.error(f"Cut and Glue failed for {recording}, returning None.")
            return None

    def cut_glue_timeslot_id(self, id: int) -> Recording | None:
        """Cuts and glues a recording from a given timeslot id."""
        ts_service = TimeslotService()
        ts = ts_service.get_timeslot(id)
        if ts is None:
            logger.info(f"Timeslot '{id}' not found, returning None.")
            return None
        logger.info(f"Timeslot '{id}' found, cut and glue {ts.start}  {ts.end}")
        return self.cut_glue(ts.start, ts.end)


@dataclass
class Metadata:
    """Specific Information about a episode/recording."""

    playlist_id: int
    show_id: int
    show_name: str
    timeslot_id: int


class MetadataService:
    """Service to retrieve Metadata information from the Engine API."""

    def metadata(self, from_date: datetime, to_date: datetime) -> Metadata:
        """Get the metadata for a given time span from the ENGINE API."""
        # TODO: check if metadata is available

        return Metadata(1, 1, "Test Show 01", 0)
