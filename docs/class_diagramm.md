# Class Diagram

This class diagram was created with `pylint`. It can be created with the following command:

```bash
cd docs
pyreverse -Smy ../src/tank_cut_glue -o mmd
```

```mermaid
classDiagram
  class cut_glue {
  }
  class timeslot {
  }
  cut_glue --> timeslot
  class datetime {
  }
  class timedelta {
  }
  class Response {
  }
  class App {
    start()
  }
  class AudioBlock {
    exists
    path
    s_time
    start_date
  }
  class CutGlueService {
    cut_glue(from_date: datetime, to_date: datetime) Recording | None
    cut_glue_timeslot_id(id: int) Recording | None
    run_sox(sox_cmd: List[str]) bool
  }
  class Metadata {
    playlist_id : int
    show_id : int
    show_name : str
    timeslot_id : int
  }
  class MetadataService {
    metadata(from_date: datetime, to_date: datetime) Metadata
  }
  class Recording {
    audio_blocks : list
    duration
    exists
    file_location
    from_date
    offset
    status
    to_date
    build_audio_blocks(from_date: datetime, to_date: datetime, interval: int) List[AudioBlock]
  }
  class SteeringRequestFailedException {
    response
  }
  class Timeslot {
    end
    id : int
    is_repetition
    memo
    note_id
    playlist_id
    schedule : int
    show : int
    start
  }
  class TimeslotService {
    steering_url
    timeslot_url
    elapsed_timeslots() List[Timeslot]
    get_start_end_from_timeslot(timeslot_id: int) Tuple[datetime, datetime] | None
    get_timeslot(timeslot_id: int) Timeslot | None
    steering_request(url, params)
  }
  datetime --* AudioBlock : start_date
  datetime --* Recording : from_date
  datetime --* Recording : to_date
  datetime --* Timeslot : start
  datetime --* Timeslot : start
  datetime --* Timeslot : end
  datetime --* Timeslot : end
  timedelta --* Response : elapsed
  datetime --* AudioBlock : start_date
  datetime --* Recording : from_date
  datetime --* Recording : to_date
  datetime --* Timeslot : start
  datetime --* Timeslot : start
  datetime --* Timeslot : end
  datetime --* Timeslot : end
  timedelta --* Response : elapsed
  Response --* SteeringRequestFailedException : response
```