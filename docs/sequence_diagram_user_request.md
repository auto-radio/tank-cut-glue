
<div class="right">

```mermaid
sequenceDiagram
    User->>+tank-cut-glue: GET recording
        tank-cut-glue->>+Cut-GlueService: cut_glue
            Cut-GlueService->>+Recording: init
                Recording->>Recording: check files      
            alt no audio blocks
                Cut-GlueService-->>tank-cut-glue: Not available
                tank-cut-glue-->>User: Recording not ready yet
            else audio blocks
            
            Cut-GlueService->>Cut-GlueService: run sox
            Cut-GlueService->>Recording: update Filelocation
            Cut-GlueService->>+MetadataService: request metadata
                MetadataService->>+EngineAPI: request playlog
                EngineAPI-->>-MetadataService: return playlog
                MetadataService->>MetadataService: extract Metadata
            MetadataService-->>-Cut-GlueService: return metadata
            Cut-GlueService->>Recording: update metadata
            Cut-GlueService->>+ExportService: export recording
                ExportService->>+TankAPI: import episode
                TankAPI-->>-ExportService: import successful
            ExportService-->>-Cut-GlueService: import successful
            Recording-->>-Cut-GlueService: recording
        Cut-GlueService-->>-tank-cut-glue: Filelocation of recording
        end
    tank-cut-glue-->>-User: Filelocation of episode
```
</div>