-include build/base.Makefile
-include build/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    lint            - verify code style"
	@echo "    spell           - check spelling of text"
	@echo "    format          - apply automatic formatting"
	@echo "    test            - run the test suite"
	@echo "    coverage        - check the coverage of the test suite"
	@echo "    log             - tail log file"
	@echo "    control         - start only the control service"
	@echo "    run.dev         - run tank-cut-glue dev server"
	@echo "    run             - run tank-cut-glue"
	@echo "    release         - tags and pushes a release with current version"
	$(call docker_help)

# Settings

AURA_TANK_CUT_GLUE_HOST ?= 127.0.0.1
AURA_TANK_CUT_GLUE_PORT ?= 8080
AURA_TANK_CUT_GLUE_CONFIG := ${CURDIR}/config/tank-cut-glue.yaml
AURA_LOGS := ${CURDIR}/logs
AURA_UID := 2872
AURA_GID := 2872
TIMEZONE := "Europe/Vienna"
AUDIO_STORE := ${CURDIR}/audio
AURA_STEERING_API := ${AURA_STEERING_API}

DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		--mount type=tmpfs,destination=/tmp \
		--device /dev/snd \
		--group-add audio \
		-e TZ=$(TIMEZONE) \
		-e LOG_LEVEL="INFO" \
		-e AURA_STEERING_API=$(AURA_STEERING_API) \
		-p 8080:8080 \
		-v "$(AUDIO_STORE)":"/var/audio" \
		-v "$(AURA_LOGS)":"/srv/logs" \
		-u $(AURA_UID):$(AURA_GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)

init.app : pyproject.toml poetry.lock
	poetry install --no-dev

init.dev : pyproject.toml poetry.lock
	poetry install

lint::
	poetry run flake8 .

spell::
	poetry run codespell $(wildcard *.md) docs src tests config contrib

format::
	poetry run isort .
	poetry run black .

coverage::
	poetry run coverage run -m pytest
	poetry run coverage report
	poetry run coverage xml
	
test::
	poetry run pytest --cov=src --junitxml=report.xml

log::
	tail -f log/tank-cut-glue.log

run::
	GUNICORN_CMD_ARGS="--bind=127.0.0.1:8080" poetry run gunicorn -c config/gunicorn.conf.py tank_cut_glue.wsgi:app

run.dev::
	poetry run start

run.prod::
	poetry run gunicorn --bind 0.0.0.0:5000 src.tank_cut_glue.app:main

control::
	poetry run control

release:: VERSION := $(shell python3 -c 'import tomli; print(tomli.load(open("pyproject.toml", "rb"))["tool"]["poetry"]["version"])')
release::
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."
